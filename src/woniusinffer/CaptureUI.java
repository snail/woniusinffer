package woniusinffer;

import java.awt.Container;
import java.awt.Insets; 
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField; 

@SuppressWarnings("serial")
public final class CaptureUI extends JFrame {

    public JTextArea mainTextArea = new JTextArea();
    public JScrollPane mainTextAreaWarp = new JScrollPane(mainTextArea);
    public JCheckBox hzcheckbox = new JCheckBox("混杂模式"); 
    public JCheckBox tcpchk = new JCheckBox("TCP");
    public JCheckBox udpchk = new JCheckBox("UDP");
    public JCheckBox arpchk = new JCheckBox("ARP");
    public JCheckBox icmpchk = new JCheckBox("ICMP");
    public JButton startButton = new JButton("开始抓包");
    public JButton stopButton = new JButton("停止抓包");
    public JButton clearButton = new JButton("清空信息");
    public JComboBox deviceList = new JComboBox();
    public JLabel deviceLabel = new JLabel("网卡列表");
    public JLabel fromelJLabel = new JLabel("From IP:");
    public JLabel tolJLabel = new JLabel("To IP:");
    public JLabel containsJLabel = new JLabel("Contains IP:");
    public JLabel contentJLabel = new JLabel("Contains Content:");
    public JLabel charsetjlable = new JLabel("Charset:");
    public JTextField from = new JTextField();
    public JTextField to = new JTextField();
    public JTextField containsjField = new JTextField();
    public JTextField contentJTextField = new JTextField();
    public JTextField charsetJTextField = new JTextField();

    public CaptureUI() {

        //frame setting
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("狂奔的蜗牛嗅探器");
        setBounds(300, 150, 695, 560);
        setResizable(false);
        //body
        Container uiBody = getContentPane();
        uiBody.setLayout(null);

        //textarea
        mainTextAreaWarp.setBounds(0, 0, 690, 390);
        mainTextArea.setBounds(0, 0, 690, 390);
//		mainTextArea.setLineWrap(true);
        mainTextArea.setMargin(new Insets(5, 5, 5, 5));
        mainTextArea.setEditable(false);
        //deviceLabel
        deviceLabel.setBounds(10, 460, 90, 25);

        //deviceList
        deviceList.setBounds(90, 460, 380, 25);
        getDeviceList();//初始化网卡列表
        //checkbox
        hzcheckbox.setSize(100, 20);
        hzcheckbox.setLocation(5, 490);
        hzcheckbox.setSelected(true);
 
        tcpchk.setSize(50, 20);
        tcpchk.setLocation(10, 430); 
        udpchk.setSize(50, 20);
        udpchk.setLocation(60, 430);
        arpchk.setSize(50, 20);
        arpchk.setLocation(110, 430);
        icmpchk.setSize(60, 20);
        icmpchk.setLocation(170, 430);
        icmpchk.setSelected(true);
        //startbutton
        startButton.setBounds(150, 490, 90, 25);
        //stopButton
        stopButton.setBounds(310, 490, 90, 25);
        fromelJLabel.setBounds(10, 400, 90, 25);
        from.setBounds(60, 400, 120, 25);
        tolJLabel.setBounds(185, 400, 90, 25);
        to.setBounds(220, 400, 120, 25);
        containsJLabel.setBounds(345, 400, 90, 25);
        containsjField.setBounds(415, 400, 120, 25);
        contentJLabel.setBounds(313, 430, 150, 25);
        contentJTextField.setBounds(415, 430, 120, 25);
        charsetjlable.setBounds(535, 430, 100, 25);
        charsetJTextField.setBounds(585, 430, 60, 25);
        //clearButton
        clearButton.setBounds(500, 460, 90, 25);
        //add component
        uiBody.add(mainTextAreaWarp);
        uiBody.add(hzcheckbox);
        uiBody.add(startButton);
        uiBody.add(stopButton);
        uiBody.add(clearButton);
        uiBody.add(deviceList);
        uiBody.add(deviceLabel);
        uiBody.add(from);
        uiBody.add(to);
        uiBody.add(fromelJLabel);
        uiBody.add(tolJLabel);
        uiBody.add(tcpchk);
        uiBody.add(udpchk);
        uiBody.add(arpchk);
        uiBody.add(icmpchk);
//        uiBody.add(allchk);
        uiBody.add(containsJLabel);
        uiBody.add(containsjField);
        uiBody.add(contentJLabel);
        uiBody.add(contentJTextField);
        uiBody.add(charsetJTextField);
        uiBody.add(charsetjlable);
        //reg listener
        startButton.addActionListener(new UIControler(this));
        stopButton.addActionListener(new UIControler(this));
        clearButton.addActionListener(new UIControler(this));
//        try {
//            try {
//                //</editor-fold>
//                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//            } catch (ClassNotFoundException ex) {
//                Logger.getLogger(CaptureUI.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } catch (Exception ex) {
//            Logger.getLogger(CaptureUI.class.getName()).log(Level.SEVERE, null, ex);
//        }
        //setVisible(true);
    }

    public static void main(String[] args) {
        CaptureUI ui = new CaptureUI();
        ui.setInitStatus();
        ui.setVisible(true);
    }

    public void getDeviceList() {
        String[] list = ArpPacketCapture.getNetInterfaceList();
        for (int i = 0; i < list.length; i++) {
            deviceList.addItem(list[i]);
        }
        if (list.length == 0) {
            addMessage("获取网卡列表出错！");
            setErrorStatus();
        }
    }
    //向文本框中显示信息

    public void addMessage(String message) {
        mainTextArea.append(message);
        mainTextArea.select(mainTextArea.getText().length(), 0);
        ArpPacketCapture.log(message);
    }
    //状态设置，这个是允许开始抓包的状态,点击了停止

    public void setCanStarStatus() {
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
        hzcheckbox.setEnabled(false);
        deviceList.setEnabled(false);
    }
    //状态设置，这个是允许停止抓包的状态,点击了开始

    public void setCanStopStatus() {
        startButton.setEnabled(false);
        stopButton.setEnabled(true);
        hzcheckbox.setEnabled(false);
        deviceList.setEnabled(false);
    }
    //状态设置，这个是出错不能就行抓包的状态

    public void setErrorStatus() {
        startButton.setEnabled(false);
        stopButton.setEnabled(false);
        hzcheckbox.setEnabled(false);
        deviceList.setEnabled(false);
    }
    //状态设置，这个是初始状态

    public void setInitStatus() {
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
        hzcheckbox.setEnabled(true);
        deviceList.setEnabled(true);
    }
}
